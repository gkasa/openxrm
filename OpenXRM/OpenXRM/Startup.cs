﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OpenXRM.Startup))]
namespace OpenXRM
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
